function separateObjectsName(objectArray){
    var separatedNames = [];
    for (var i=0;i<objectArray.length;i++) {
        if (objectArray[i].name.includes("]")){
            strBeforeArrayTheName = objectArray[i].name.indexOf(']');
            strIn = objectArray[i].name.substr(0, strBeforeArrayTheName);
        }

        if ( !separatedNames.includes(strIn) ) {
            separatedNames.push(strIn);
        }
    }
    return separatedNames;
}

function separateObjectProperties(objectArray){
    var separatedProperties = [];
    for (var i=0;i<objectArray.length;i++) {
        if (objectArray[i].name.includes("[")){
            strOutArrayTheName = objectArray[i].name.indexOf('[');
            strOutArrayKeyName = objectArray[i].name.length;
            strOut = objectArray[i].name.substring(strOutArrayTheName+1, strOutArrayKeyName-1 );

            strInArrayTheName = strOut.indexOf('[');
            strInArrayKeyName = strOut.length;
            strIn = strOut.substring(strInArrayTheName+1, strOutArrayKeyName );
        }

        if ( !separatedProperties.includes(strIn) ) {
            separatedProperties.push(strIn);
        }
    }
    return separatedProperties;
}

function getObjectValues(objectArray, objectName){
    var separatedValues = [];
    for (var i=0;i<objectArray.length;i++) {
        if (objectArray[i].name.includes(objectName)){
           separatedValues.push(objectArray[i].value);
        }
    }
    return separatedValues;
}

function groupInputIntoObjects(InputArray){
    var namesForObjects = separateObjectsName(InputArray);
    var propertiesForObjects = separateObjectProperties(InputArray);
    var preparedObjects = [];

    for (var i=0;i<namesForObjects.length;i++){
        var valuesOfObject = getObjectValues(InputArray ,namesForObjects[i]);
        this[ namesForObjects[i] ] = {};

        for(var j=0;j<propertiesForObjects.length;j++){
            this[ namesForObjects[i] ][propertiesForObjects[j]] = valuesOfObject[j];
        }
    preparedObjects.push(this[ namesForObjects[i] ]);
    }
    return preparedObjects;
}

function setSimpleCartOrder(InputOrder){
    var objArrOrders = groupInputIntoObjects(InputOrder);

    reqServerJSON({actionName:'countPrice',dataData:objArrOrders},
        function () {
            var jsonResponse = JSON.parse(this.responseText);
            console.log(jsonResponse);

            for (var i=0; i<objArrOrders.length; i++){
                var orderObj =  objArrOrders[i];
                simpleCart.add({
                    name: 'Metoda: '+orderObj["method"]+' ,rozmiar: '+ orderObj["size1"]+' x '+orderObj["size2"],
                    price: jsonResponse[i]["price"] ,
                    size: orderObj["size1"]+orderObj["size2"] ,
                    quantity: 1
                });
            }
        }
    );
}
<?php

$json = json_decode($_POST['json']);
$jsonData = $json->dataData;

$price = new OrderPrice();

    //var_dump($jsonData);
    foreach ($jsonData as $jsonDataItem) {
        if (empty($jsonDataItem->size1)) {
            $jsonDataItem->size1 = 0;
        }
        if (empty($jsonDataItem->size2)) {
            $jsonDataItem->size2 = 0;
        }

        $jsonDataItem->price = $price->calculate( ['size1'=>$jsonDataItem->size1, 'size2'=>$jsonDataItem->size2] );
    }

echo json_encode($jsonData);

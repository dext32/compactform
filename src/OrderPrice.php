<?php
/**
 * Created by PhpStorm.
 * User: DOM
 * Date: 23.03.2019
 * Time: 21:08
 */

class OrderPrice
{

    public function calculate(Array $orderData){
        if ( !array_key_exists('size1',$orderData) || !array_key_exists('size2',$orderData))
            throw new Exception('Height and width must be set.');

        return (($orderData['size1'] * 10) * 2) + (($orderData['size2'] * 10) * 2);
    }

}
<?php
/**
 * Created by PhpStorm.
 * User: DOM
 * Date: 23.03.2019
 * Time: 20:20
 */

class NewOrder
{
    private $objOrder;
    private $repoOrder;
    private $price;

    public function __construct(OrderEntity $objOrder,OrderRepository $repoOrder, OrderPrice $price)
    {
        $this->objOrder = $objOrder;
        $this->repoOrder = $repoOrder;
        $this->price = $price;
    }

    public function addNewOrder(Array $FormOrderData){
        $repoOrder = $this->repoOrder;

        $return = array();

        $setOrderGroupId = $repoOrder->generateGroupOrderId();
        
        foreach ($FormOrderData as $order){
            $objOrder = new OrderEntity();

            if ( empty($order['size1']) || empty($order['size2']) ) {
                if ( empty($order['size1']) )
                    $order['size1'] = 0;
                if ( empty($order['size2']) )
                    $order['size2'] = 0;
            } else {
                $order['size1'] = (int) filter_var(round($order['size1']), FILTER_SANITIZE_NUMBER_INT);
                $order['size2'] = (int) filter_var(round($order['size2']), FILTER_SANITIZE_NUMBER_INT);
            }
            $setPrice = $this->price->calculate(
                [
                    'size1'=>$order['size1'],
                    'size2'=>$order['size2']
                ]
            );
            $setName = "Metoda - ".$order['method'].", rozmiar: ".$order['size1']."cm / ".$order['size2']."cm";
            $objOrder -> setOrderGroup($setOrderGroupId);
            $objOrder -> setName($setName);
            $objOrder -> setSize1($order['size1']);
            $objOrder -> setSize2($order['size2']);
            $objOrder -> setPrice($setPrice);
            $objOrder -> setMethod($order['method']);
            $objOrder -> setMaterial($order['material']);
            $objOrder -> setReview($order['review']);

            $repoOrder->addNewOrder($objOrder);
            array_push($return ,$objOrder);
        }

        return $return;
    }


}
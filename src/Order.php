<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 25.03.2019
 * Time: 15:27
 */

class Order extends OrderEntity
{

    private $objOrder;
    private $repoOrder;

    public function __construct(OrderEntity $objOrder,OrderRepository $repoOrder)
    {
        $this->objOrder = $objOrder;
        $this->repoOrder = $repoOrder;
    }

    public function getData(){
        return $this->repoOrder->getData($this);
    }

    public function getGroupData(){
        return $this->repoOrder->getGroupData($this);
    }

}
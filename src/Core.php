<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 22.03.2019
 * Time: 14:18
 */

class Core
{
    private $di_container;

    public function connectionDb(){

    }

    public function buildDi(){
        $builder = new DI\ContainerBuilder();
        $this->di_container = $builder->build();
        return $builder->build();
    }

    public function runApplication(){
        // show error reporting
        error_reporting(E_ALL);

        // start php session
        session_start();
    }

    public function renderView(){
        $container = $this->di_container;
        include 'template/front.php';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 22.03.2019
 * Time: 14:03
 */


class OrderEntity
{
    private $id;
    private $order_group;
    private $name;
    private $size1;
    private $size2;
    private $price;
    private $method;
    private $material;
    private $review;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOrderGroup()
    {
        return $this->order_group;
    }

    /**
     * @param mixed $order_group
     */
    public function setOrderGroup($order_group)
    {
        $this->order_group = $order_group;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSize1()
    {
        return $this->size1;
    }

    /**
     * @param mixed $size1
     */
    public function setSize1($size1)
    {
        $this->size1 = $size1;
    }

    /**
     * @return mixed
     */
    public function getSize2()
    {
        return $this->size2;
    }

    /**
     * @param mixed $size2
     */
    public function setSize2($size2)
    {
        $this->size2 = $size2;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * @param mixed $method
     */
    public function setMethod($method)
    {
        $this->method = $method;
    }

    /**
     * @return mixed
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * @param mixed $material
     */
    public function setMaterial($material)
    {
        $this->material = $material;
    }

    /**
     * @return mixed
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * @param mixed $review
     */
    public function setReview($review)
    {
        $this->review = $review;
    }


}
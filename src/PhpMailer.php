<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 31.03.2019
 * Time: 22:39
 */

class PhpMailer
{

    public $mail;
    private $mail_to;
    private $subject;
    private $body;
    private $altbody;

    public function __construct(\PHPMailer\PHPMailer\PHPMailer $mail)
    {
        $this->mail = $mail;
    }

    /**
     * @return mixed
     */
    public function getMailTo()
    {
        return $this->mail_to;
    }

    /**
     * @param mixed $mail_to
     */
    public function setMailTo($mail_to)
    {
        $this->mail_to = $mail_to;
    }

    /**
     * @return mixed
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @param mixed $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }

    /**
     * @return mixed
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param mixed $body
     */
    public function setBody($body)
    {
        $this->body = $body;
    }

    /**
     * @return mixed
     */
    public function getAltbody()
    {
        return $this->altbody;
    }

    /**
     * @param mixed $altbody
     */
    public function setAltbody($altbody)
    {
        $this->altbody = $altbody;
    }

    public function send(){
        try {
            //Server settings
            $this->mail->SMTPDebug = 2;                                 // Enable verbose debug output
            $this->mail->isSMTP();                                      // Set mailer to use SMTP
            $this->mail->Host = 'smtp.office365.com';  // Specify main and backup SMTP servers
            $this->mail->SMTPAuth = true;                               // Enable SMTP authentication
            $this->mail->Username = '';                 // SMTP username
            $this->mail->Password = '';                           // SMTP password
            $this->mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
            $this->mail->Port = 587;                                    // TCP port to connect to

            //Recipients
            $this->mail->setFrom('webservices36@outlook.com');
            $this->mail->addAddress($this->mail_to);     // Add a recipient

            //Content
            $this->mail->isHTML(true);                                  // Set email format to HTML
            $this->mail->Subject = $this->subject;
            $this->mail->Body    = $this->body;
            $this->mail->AltBody = $this->altbody;

            $this->mail->send();
            return true;
        } catch (Exception $e) {
            echo 'Message could not be sent. Mailer Error: ', $this->mail->ErrorInfo;
        }
    }

}
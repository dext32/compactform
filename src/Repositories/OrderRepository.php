<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 22.03.2019
 * Time: 14:10
 */

class OrderRepository {

    private $pdo;

    public function __construct(PdoMysql $pdo)
    {
        $this->pdo = $pdo->pdo;
    }

    public function addNewOrder(OrderEntity $order){
        $query = $this->pdo->prepare("INSERT INTO orders (order_group,name,size1,size2,price,method,material,review) VALUES (:order_group,:name,:size1,:size2,:price,:method,:material,:review)");
        $Order['order_group'] = $order->getOrderGroup();
        $query->bindParam(':order_group',$Order['order_group'], PDO::PARAM_INT);
        $Order['name'] = $order->getName();
        $query->bindParam(':name',$Order['name'], PDO::PARAM_STR);
        $Order['size1'] = $order->getSize1();
        $query->bindParam(':size1',$Order['size1'], PDO::PARAM_STR);
        $Order['size2'] = $order->getSize2();
        $query->bindParam(':size2',$Order['size2'], PDO::PARAM_STR);
        $Order['price'] = $order->getPrice();
        $query->bindParam(':price',$Order['price'], PDO::PARAM_STR);
        $Order['method'] = $order->getMethod();
        $query->bindParam(':method',$Order['method'], PDO::PARAM_STR);
        $Order['material'] = $order->getMaterial();
        $query->bindParam(':material',$Order['material'], PDO::PARAM_STR);
        $Order['review'] = $order->getReview();
        $query->bindParam(':review',$Order['review'], PDO::PARAM_STR);
        $query->execute();
    }

    public function generateGroupOrderId(){
        $query = $this->pdo->prepare("SELECT MAX(`order_group`)+1 AS LatestGroup FROM `orders`");
        $query -> execute();
        $result = $query->fetch();
        return $result['LatestGroup'];
    }

    public function getData(OrderEntity $order){
        $Order['id'] = $order->getId();
        $query = $this->pdo->prepare('SELECT * FROM orders WHERE id = :orderId');
        $query -> bindParam(':orderId',$Order['id'], PDO::PARAM_INT);
        $query -> execute();
        return $query->fetch();
    }

    public function getGroupData(OrderEntity $order){
        $Order['order_group'] = $order->getOrderGroup();
        $query = $this->pdo->prepare('SELECT * FROM orders WHERE order_group = :order_group');
        $query -> bindParam(':order_group',$Order['order_group'], PDO::PARAM_INT);
        $query -> execute();
        return $query->fetchAll();
    }

}
-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 28 Mar 2019, 16:25
-- Wersja serwera: 10.1.35-MariaDB
-- Wersja PHP: 7.2.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `compactform`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orders`
--

CREATE TABLE `orders` (
  `id` int(255) NOT NULL,
  `order_group` int(55) NOT NULL,
  `name` varchar(255) NOT NULL,
  `size1` varchar(255) NOT NULL,
  `size2` varchar(255) NOT NULL,
  `method` varchar(255) NOT NULL,
  `material` varchar(255) NOT NULL,
  `review` text NOT NULL,
  `price` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Zrzut danych tabeli `orders`
--

INSERT INTO `orders` (`id`, `order_group`, `name`, `size1`, `size2`, `method`, `material`, `review`, `price`) VALUES
(1, 1, 'ramka', '10', '10', 'ramka', '2-36', '', '600'),
(2, 1, 'overlock', '20', '20', 'overlock', '2-41', '', '800'),
(3, 2, 'bordiura', '10', '10', 'bordiura', '4-12', '', '400'),
(4, 0, 'Metoda - overlock tasma, rozmiar: 10cm / 20cm', '10', '20', 'overlock tasma', '1', '', '600'),
(5, 0, 'Metoda - overlock tasma, rozmiar: 10cm / 20cm', '10', '20', 'overlock tasma', '1', '', '600'),
(6, 3, 'Metoda - other-method, rozmiar: 0cm / 0cm', '0', '0', 'other-method', '0', 'ASDFA', '0'),
(7, 4, 'Metoda - oklejenie ramka, rozmiar: 10cmcm / 20cmcm', '10cm', '20cm', 'oklejenie ramka', '1', '', '600'),
(8, 5, 'Metoda - oklejenie ramka, rozmiar: 10cmcm / 10cmcm', '10cm', '10cm', 'oklejenie ramka', '1', '', '400'),
(9, 5, 'Metoda - overlock tasma, rozmiar: 20cmcm / 20cmcm', '20cm', '20cm', 'overlock tasma', '1', '', '800'),
(10, 6, 'Metoda - oklejenie ramka, rozmiar: 10cmcm / 10cmcm', '10cm', '10cm', 'oklejenie ramka', '1', '', '400'),
(11, 6, 'Metoda - overlock tasma, rozmiar: 20cmcm / 20cmcm', '20cm', '20cm', 'overlock tasma', '1', '', '800'),
(12, 7, 'Metoda - oklejenie ramka, rozmiar: 10cmcm / 10cmcm', '10', '10', 'oklejenie ramka', '1', '', '400'),
(13, 7, 'Metoda - overlock tasma, rozmiar: 20cmcm / 20cmcm', '20', '20', 'overlock tasma', '1', '', '800'),
(14, 8, 'Metoda - oklejenie ramka, rozmiar: 10cm / 10cm', '10', '10', 'oklejenie ramka', '1', '', '400'),
(15, 8, 'Metoda - overlock tasma, rozmiar: 20cm / 20cm', '20', '20', 'overlock tasma', '1', '', '800'),
(16, 9, 'Metoda - podwiniecie, rozmiar: 105cm / 105cm', '105', '105', 'podwiniecie', '1', '', '4200'),
(17, 10, 'Metoda - overlock bordiura, rozmiar: 105cm / 105cm', '105', '105', 'overlock bordiura', '1', '', '4200'),
(18, 10, 'Metoda - overlock bordiura, rozmiar: 0cm / 0cm', '0', '0', 'overlock bordiura', '1', '', '0'),
(19, 11, 'Metoda - oklejenie ramka, rozmiar: 10cm / 105cm', '10', '105', 'oklejenie ramka', '1', '', '2300'),
(20, 12, 'Metoda - oklejenie ramka, rozmiar: 10cm / 105cm', '10', '105', 'oklejenie ramka', '1', '', '2300'),
(21, 13, 'Metoda - oklejenie ramka, rozmiar: 10cm / 11cm', '10', '11', 'oklejenie ramka', '1', '', '420'),
(22, 14, 'Metoda - overlock bordiura, rozmiar: 24cm / 26cm', '24', '26', 'overlock bordiura', '1', '', '1000');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `firstname` varchar(32) NOT NULL,
  `lastname` varchar(32) NOT NULL,
  `email` varchar(64) NOT NULL,
  `contact_number` varchar(64) NOT NULL,
  `address` text NOT NULL,
  `password` varchar(512) NOT NULL,
  `access_level` varchar(16) NOT NULL,
  `access_code` text NOT NULL,
  `status` int(11) NOT NULL COMMENT '0=pending,1=confirmed',
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='admin and customer users';

--
-- Zrzut danych tabeli `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `contact_number`, `address`, `password`, `access_level`, `access_code`, `status`, `created`, `modified`) VALUES
(1, 'Darwin', 'Dalisay', 'darwin@example.com', '9331868359', 'Blk 24 A Lot 6 Ph 3\r\nPeace Village, San Luis', '$2y$10$tLq9lTKDUt7EyTFhxL0QHuen/BgO9YQzFYTUyH50kJXLJ.ISO3HAO', 'Customer', 'ILXFBdMAbHVrJswNDnm231cziO8FZomn', 1, '2014-10-29 17:31:09', '2016-06-13 16:18:25'),
(2, 'Mike', 'Dalisay', 'mike@example.com', '0999999999', 'Blk. 24 A, Lot 6, Ph. 3, Peace Village', '$2y$10$AUBptrm9sQF696zr8Hv31On3x4wqnTihdCLocZmGLbiDvyLpyokL.', 'Admin', '', 1, '0000-00-00 00:00:00', '2016-06-13 16:17:47'),
(3, 'Bartoszewski', 'Diuk', 'diuk36@gmail.com', '0784834939', 'Nowowiejska', '$2y$10$Gu3qHZBqB8zewkkNyJ3CZ.4.Lw7zkWb4rrERcEHTdF.cCqGMbHpFe', 'Admin', '', 1, '2019-03-28 04:23:06', '2019-03-28 03:25:37');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT dla tabeli `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

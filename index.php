<?php

    require_once 'vendor/autoload.php';
    include 'autoload.php';

    $core = new Core();

    $container = $core->buildDi();

    $core -> runApplication();

    $core -> renderView();

?>

<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 27.03.2019
 * Time: 13:40
 */

class utils
{
    public $mailer;

    public function __construct(PhpMailer $mailer)
    {
        $this->mailer = $mailer;
    }

    function getToken($length=32){
        $token = "";
        $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
        $codeAlphabet.= "0123456789";
        for($i=0;$i<$length;$i++){
            $token .= $codeAlphabet[$this->crypto_rand_secure(0,strlen($codeAlphabet))];
        }
        return $token;
    }

    function crypto_rand_secure($min, $max) {
        $range = $max - $min;
        if ($range < 0) return $min; // not so random...
        $log = log($range, 2);
        $bytes = (int) ($log / 8) + 1; // length in bytes
        $bits = (int) $log + 1; // length in bits
        $filter = (int) (1 << $bits) - 1; // set all lower bits to 1
        do {
            $rnd = hexdec(bin2hex(openssl_random_pseudo_bytes($bytes)));
            $rnd = $rnd & $filter; // discard irrelevant bits
        } while ($rnd >= $range);
        return $min + $rnd;
    }

    public function sendEmailViaPhpMail($send_to_email, $subject, $body){

        $mailer = $this->mailer;
        $mailer -> setMailTo($send_to_email);
        $mailer -> setSubject($subject);
        $mailer -> setBody($body);

        if($mailer->send()){
            echo "<pre>";
            echo print_r($mailer->mail);
            echo "</pre>";
            return true;
        }else{
            return false;
        }
    }

}
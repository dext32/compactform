<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 27.03.2019
 * Time: 14:20
 */

// login checker for 'admin' access level

// if the session value is empty, he is not yet logged in, redirect him to login page
if(empty($_SESSION['logged_in'])){
    header("Location: {$home_url}index.php?p=login&action=not_yet_logged_in");
}

// if access level was not 'Admin', redirect him to login page
else if($_SESSION['access_level']!="Admin"){
    header("Location: {$home_url}index.php?p=login&action=not_admin");
}

else{
    // no problem, stay on current page
}

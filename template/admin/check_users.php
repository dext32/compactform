<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 27.03.2019
 * Time: 18:39
 */

// check if logged in as admin
include_once $template_admin."login_checker.php";

$user = $container->get('UserEntity');

// set page title
$page_title = "Users";

echo "<div class='col-md-12'>";

// read all users from the database
$stmt = $user->readAll($from_record_num, $records_per_page);

// count retrieved users
$num = $stmt->rowCount();

// to identify page for paging
$page_url="index.php?p=admin&a=list_users&";

// include products table HTML template
include_once "read_users_template.php";

echo "</div>";
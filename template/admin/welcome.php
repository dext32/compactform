<?php
/**
 * Created by PhpStorm.
 * User: MSI
 * Date: 27.03.2019
 * Time: 13:56
 */

// check if logged in as admin
require "template/admin/login_checker.php";

// set page title
$page_title="Admin Index";

echo "<div class='col-md-12'>";

// get parameter values, and to prevent undefined index notice
$action = isset($_GET['action']) ? $_GET['action'] : "";

// tell the user he's already logged in
    if($action=='already_logged_in'){
        echo "<div class='alert alert-info'>";
        echo "<strong>You</strong> are already logged in.";
        echo "</div>";
    }

    else if($action=='logged_in_as_admin'){
        echo "<div class='alert alert-info'>";
        echo "<strong>You</strong> are logged in as admin.";
        echo "</div>";
    }

echo "<div class='alert alert-info'>";
echo "Contents of your admin section will be here.";
echo "</div>";

echo "</div>";

<?php

if (isset($_POST['json'])) {
    require 'jscript/json_response/json_response.php';
} else {
    include 'template/login_script.php';

    include 'template/header.php';

    include 'template/login_toolbar.php';
    include_once "template/login_checker.php";

    if(isset($_GET['p'])){

        switch($_GET['p']){

            case 'login':{
                include 'template/user/login.php';
            } break;

            case 'welcome':{
                if (isset($_GET['action'])) {
                    include 'template/user/welcome.php';
                }
            } break;

            case 'logout':{
                include 'template/user/logout.php';
            } break;

            case 'register':{
                include 'template/user/register.php';
            } break;

            case 'verify':{
                include $template_user.'verify.php';
            } break;

            case 'admin':{
                include 'template/admin/login_checker.php';
                include_once "template/admin/navbar.php";

                if (isset($_GET['a'])) {
                    switch ($_GET['a']){
                        case 'list_users':{
                            include $template_admin.'check_users.php';
                        } break;
                    }
                } else {
                    include 'template/admin/welcome.php';
                }

            } break;

            case 'order_proceed':{
                $data = $_POST['order'];

                $order = $container->get('NewOrder');
                $order->addNewOrder($_POST['order']);
            } break;

            case 'order_details':{
                $order = $container->get('Order');
                $order->setId($_GET['id']);
                $data = $order->getData();
            } break;

            case 'order_group_details':{
                $order = $container->get('Order');
                $order->setOrderGroup($_GET['id']);
                $data = $order->getGroupData($order);
            } break;

        }

        if(isset($data)) {echo "<pre>"; echo print_r($data); echo "</pre>";}

    }


    include "template/new_order_lite.php";


    include 'template/footer.php';
}

?>

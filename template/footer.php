
<!-- jQuery library -->

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>

<!-- Bootstrap JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<!-- Scripts for compactForm -->
<script src="jscript/compactform.js"></script>

<!-- Script for Ajax/json responses -->
<script src="jscript/jsonQuery.js"></script>

<!-- Scripts for dynamicList -->
<script type="text/javascript" src="jscript/dynamicList/js/quirksmode.js"></script>
<script type="text/javascript" src="jscript/dynamicList/js/renderList.js"></script>

<script src="jscript/jquery-1.10.2.js"></script>
<script>
    function swapFormInputBySelect(elem){
        if(elem.value == 'other-method') {
            $(elem).parent().children( ".standard-method" ).css( "display", "none" );
            $(elem).parent().children( ".custom-method" ).css( "display", "initial" );
        } else {
            $(elem).parent().children( ".standard-method" ).css( "display", "initial" );
            $(elem).parent().children( ".custom-method" ).css( "display", "none" );
        }
    }

    $( "#testSerialize" ).click(function() {
        console.log( $( "#order-data" ).serializeArray() );

        setSimpleCartOrder($( "#order-data" ).serializeArray());
    });
</script>

<script src="jscript/simplecart/simpleCart.js"></script>
<script>
    simpleCart({
        checkout: {
            type: "SendForm" ,
            url: "index.php" ,
            method: "POST"
        }
    });
</script>

</body>

<footer></footer>

</html>
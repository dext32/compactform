<?php

$user = $container->get('UserEntity');

// set access code
$user->access_code=isset($_GET['access_code']) ? $_GET['access_code'] : "";

// verify if access code exists
if(empty($user->access_code)){
    die("ERROR: Access code not found.");
}

// redirect to login
else{

    // update status
    $user->status=1;
    $user->updateStatusByAccessCode();

    // and the redirect
    header("Location: {$home_url}index.php?p=login&action=email_verified");
}
?>
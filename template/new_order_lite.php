<?php
    var_dump($_POST);
?>

<style>
    #order-item-data label::before, #order-item-data select::before {content: '\A'; white-space: pre;}
    #order-item-data .custom-method {display: none;}
</style>

<body>
<div id="readroot" style="display: none">

    <select name="order[][method]" onchange="swapFormInputBySelect(this)">
        <option value="0">Sposób obszycia</option>
        <option value="oklejenie ramka">Oklejenie ramką</option>
        <option value="podwiniecie">Podwinięcie z podkładem antypoślizgowym</option>
        <option value="overlock bordiura">Overlock bordiurą</option>
        <option value="overlock tasma">Overlock taśmą</option>
        <option value="other-method">Inne</option>
    </select>

    <label id="lbl_order_size1" for="order_size1" class="standard-method">Długość: </label>
    <input type="text" name="order[][size1]" id="order_size1" class="standard-method">
    <label id="lbl_order_size2" for="order_size2" class="standard-method">Szerokość: </label>
    <input type="text" name="order[][size2]" id="order_size2" class="standard-method">

    <label id="lbl_order_material" for="order_material" class="standard-method"></label>
    <select name="order[][material]" class="standard-method" id="order_material">
        <option value="0">Oznaczenie wzornika</option>
        <option value="1">1</option>
    </select>

    <label for="order_text" class="custom-method">Treść opisu wykonania dywanu</label><br>
    <textarea rows="5" cols="20" name="order[][review]" id="order_text" class="custom-method"></textarea>

    <input type="button" value="Usuń wykonanie"
           onclick="this.parentNode.parentNode.removeChild(this.parentNode);" /><br /><br />
</div>

<form method="post" action="index.php?p=order_proceed" id="order-data">

    <span id="writeroot"></span>

    <input type="button" id="moreFields" value="Dodaj wykonanie" />
    <input type="button" value="Test serialize" id="testSerialize">
    <input type="submit" value="Przejdź dalej" />

</form>
<br /><br />

<div class="simpleCart_items"></div><br /><br />
<a href="javascript:;" class="simpleCart_checkout">Zamów</a>
